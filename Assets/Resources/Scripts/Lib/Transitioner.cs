﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

///Versão 0.92 - Última atualização 01/07/2016 - 16:00
///Tween de posição, rotação, escala, tamanho e alfa
///Tocar áudio ao final do tween
///Retem o movimento dos scrolls até o final do tween (+procura por inativos também)
///Na falta de canvas group, procura por uma imagem para a realização do alfa
///Adição da função loop e ping-pong
///Play On Awake
///Varia a duração e o delay
///
///Melhorias:
///*Posicionar de acordo com o tamanho do parente e/ou da tela
///*Rotação sem ser local
///*Não reutilizar Random Delay quando estiver tocando em loop?
///*Opção para utilizar o delta time geral ou algum específico

public class Transitioner : MonoBehaviour
{
    public enum WrapMode
    {
        Default,
        Boomerang,
        Loop,
        PingPong,
    }

    public WrapMode wrapMode;

    public bool TweenInAction { get { return tweensExecuting > 0; } }

    public GameObject objectToAffect;

    public bool deactivateAfter, playOnAwake, ignorePosition = true, ignoreRotation=true, ignoreScale=true, ignoreSize=true, ignoreAlpha=true;

    public TweenParameters globalTween;

    public TransitTo position, rotation, scale, size;
    public TransitAlpha alpha;

    public AudioSource audioSource;
    public float audioDelay;

    //Editor
    public bool foldPosition = true, foldRotation = true, foldScale = true, foldAlpha = true, foldSize = true, foldGlobalTween, foldAudio;
    public int tweensExecuting;

    public delegate void TransitionComplete();
    public TransitionComplete onCompleteMove, onCompleteRotate, onCompleteScale, onCompleteAlpha, oncompleteSize, onCompleteAll;

    public RectTransform rectTransform;

    private CanvasGroup _canvasGroup;
    private Image _image;
    private Color _imageColorInitial, _imageColorFinal;
    private Renderer[] _renderers;

    public bool isOpen;

    private void Awake()
    {
        onCompleteMove += CompleteMove;
        onCompleteRotate += CompleteRotate;
        onCompleteScale += CompleteScale;
        onCompleteAlpha += CompleteAlpha;
        oncompleteSize += CompleteSize;
        onCompleteAll += Finish;

        if (playOnAwake)
            Play();
    }
    public void Play()
    {
        switch (wrapMode)
        {
            case WrapMode.Default:
                ChangeState();
                break;
            case WrapMode.Boomerang:
                if (isOpen)
                    onCompleteAll -= Play;
                else
                    onCompleteAll += Play;

                ChangeState();
                break;
            case WrapMode.Loop:
                if (isOpen)
                {
                    Close();
                    onCompleteAll += CloseSingle;
                }
                else
                {
                    Open();
                    onCompleteAll += Open;
                }
                break;
            case WrapMode.PingPong:
                ChangeState();

                onCompleteAll -= Play; // To avoid repetition
                onCompleteAll += Play;
                break;
            default:
                break;
        }
    }
    public void Open()
    {
        deactivateAfter = false;

        isOpen = true;

        StartTransition(false);
    }
    public void Stop()
    {
        LeanTween.cancel(objectToAffect);
    }
    private void CloseSingle()
    {
        Close();
    }
    public void Close(bool deactivate = true)
    {
        isOpen = false;

        deactivateAfter = deactivate;

        StartTransition(true);
    }
    public void ChangeState()
    {
        if (isOpen)
            Close(deactivateAfter);
        else
            Open();
    }
    public void StartTransition(bool reverse = false)
    {
        if (objectToAffect)
        {
            tweensExecuting = 0;

            objectToAffect.SetActive(true);

            if (!rectTransform)
            {
                rectTransform = objectToAffect.GetComponent<RectTransform>();
            }

            if (rectTransform)
            {
                var scrolls = rectTransform.GetComponentsInChildren<ScrollRect>(true);

                for (int i = 0; i < scrolls.Length; i++)
                {
                    scrolls[i].enabled = false;
                }

                if (_canvasGroup == null)
                    _canvasGroup = rectTransform.GetComponent<CanvasGroup>();
                if (_canvasGroup == null && _image == null)
                {
                    _image = rectTransform.GetComponent<Image>();

                    if (_image)
                    {
                        _imageColorInitial = _image.color;
                        _imageColorInitial.a = alpha.initialState;

                        _imageColorFinal = _image.color;
                        _imageColorFinal.a = alpha.finalState;
                    }
                }
            }
            else if (_renderers == null)
            {
                _renderers = GetComponentsInChildren<Renderer>();
            }

            if (audioSource)
            {
                StartCoroutine(PlayAudio());
            }

            #region Position
            if (!ignorePosition)
            {
                tweensExecuting++;

                if (position.ignoreInitialState)
                {
                    if (!reverse)
                    {
                        if (rectTransform)
                        {
                            position.initialState = rectTransform.anchoredPosition;
                        }
                        else
                        {
                            if (position.localSpace)
                                position.initialState = transform.localPosition;
                            else
                                position.initialState = transform.position;
                        }
                    }

                }
                else
                {
                    if (rectTransform)
                    {
                        rectTransform.anchoredPosition = reverse ? position.finalState : position.initialState;
                    }
                    else
                    {
                        if (position.localSpace)
                            transform.localPosition = reverse ? position.finalState : position.initialState;
                        else
                            transform.position = reverse ? position.finalState : position.initialState;
                    }
                }

                if (position.useGlobalTween)
                {
                    position.tween.Copy(globalTween);
                }

                if (rectTransform == null)
                {
                    if (position.localSpace)
                        LeanTween.moveLocal(objectToAffect, reverse ? position.initialState : position.finalState
                            , position.tween.randomDuration ? Random.Range(position.tween.minRandDuration, position.tween.maxRandDuration) : position.tween.duration)
                            .setDelay(position.tween.randomDelay ? Random.Range(position.tween.minRandDelay, position.tween.maxRandDelay) : position.tween.delay)
                            .setEase(position.tween.type)
                            .setOnComplete(() => onCompleteMove());
                    else
                        LeanTween.move(objectToAffect, reverse ? position.initialState : position.finalState
                            , position.tween.randomDuration ? Random.Range(position.tween.minRandDuration, position.tween.maxRandDuration) : position.tween.duration)
                            .setDelay(position.tween.randomDelay ? Random.Range(position.tween.minRandDelay, position.tween.maxRandDelay) : position.tween.delay)
                            .setEase(position.tween.type)
                            .setOnComplete(() => onCompleteMove());
                }
                else
                {
                    LeanTween.move(rectTransform, reverse ? position.initialState : position.finalState
                        , position.tween.randomDuration ? Random.Range(position.tween.minRandDuration, position.tween.maxRandDuration) : position.tween.duration)
                        .setDelay(position.tween.randomDelay ? Random.Range(position.tween.minRandDelay, position.tween.maxRandDelay) :  position.tween.delay)
                        .setEase(position.tween.type)
                        .setOnComplete(() => onCompleteMove());
                }
            }
            #endregion

            #region Rotation
            if (!ignoreRotation)
            {
                tweensExecuting++;

                if (!rotation.ignoreInitialState)
                {
                    if (rectTransform)
                    {
                        if (rotation.localSpace)
                            rectTransform.localRotation = Quaternion.Euler(reverse ? rotation.finalState : rotation.initialState);
                        else
                            rectTransform.rotation = Quaternion.Euler(reverse ? rotation.finalState : rotation.initialState);
                    }
                    else
                    {
                        if (rotation.localSpace)
                            transform.localRotation = Quaternion.Euler(reverse ? rotation.finalState : rotation.initialState);
                        else
                            transform.rotation = Quaternion.Euler(reverse ? rotation.finalState : rotation.initialState);
                    }
                }

                if (rotation.useGlobalTween)
                {
                    rotation.tween.Copy(globalTween);
                }

                if (rotation.localSpace)
                    LeanTween.rotateLocal(objectToAffect, reverse ? rotation.initialState : rotation.finalState
                        , rotation.tween.randomDuration ? Random.Range(rotation.tween.minRandDuration, rotation.tween.maxRandDuration) : rotation.tween.duration)
                        .setDelay(rotation.tween.randomDelay ? Random.Range(rotation.tween.minRandDelay, rotation.tween.maxRandDelay) : rotation.tween.delay)
                        .setEase(rotation.tween.type)
                        .setOnComplete(() => onCompleteRotate());
                else
                    LeanTween.rotate(objectToAffect, reverse ? rotation.initialState : rotation.finalState
                        , rotation.tween.randomDuration ? Random.Range(rotation.tween.minRandDuration, rotation.tween.maxRandDuration) : rotation.tween.duration)
                        .setDelay(rotation.tween.randomDelay ? Random.Range(rotation.tween.minRandDelay, rotation.tween.maxRandDelay) : rotation.tween.delay)
                        .setEase(rotation.tween.type)
                        .setOnComplete(() => onCompleteRotate());
            }
            #endregion

            #region Scale
            if (!ignoreScale)
            {
                tweensExecuting++;

                if (!scale.ignoreInitialState)
                {
                    if (rectTransform)
                    {
                        rectTransform.localScale = reverse ? scale.finalState : scale.initialState;
                    }
                    else
                    {
                        transform.localScale = reverse ? scale.finalState : scale.initialState;
                    }
                }

                if (scale.useGlobalTween)
                {
                    scale.tween.Copy(globalTween);
                }

                LeanTween.scale(objectToAffect, reverse ? scale.initialState : scale.finalState
                    , scale.tween.randomDuration ? Random.Range(alpha.tween.minRandDuration, scale.tween.maxRandDuration) : scale.tween.duration)
                    .setDelay(scale.tween.randomDelay ? Random.Range(scale.tween.minRandDelay, scale.tween.maxRandDelay) : scale.tween.delay)
                    .setEase(scale.tween.type)
                    .setOnComplete(() => onCompleteScale());
            }
            #endregion

            #region Alpha
            if (!ignoreAlpha)
            {
                tweensExecuting++;

                if (!alpha.ignoreInitialState)
                {
                    if (rectTransform)
                    {
                        if (_canvasGroup)
                            _canvasGroup.alpha = reverse ? alpha.finalState : alpha.initialState;
                        else if (_image)
                            _image.color = reverse ? _imageColorFinal : _imageColorInitial;
                    }
                    else
                    {
                        for (int i = 0; i < _renderers.Length; i++)
                        {
                            for (int j = 0; j < _renderers[i].materials.Length; j++)
                            {
                                try
                                {
                                    var c = _renderers[i].materials[j].color;
                                    c.a = reverse ? alpha.finalState : alpha.initialState;
                                    _renderers[i].materials[j].color = c;
                                }
                                catch (System.Exception e)
                                {
                                    Debug.Log(e);
                                }
                            }
                        }
                    }
                }

                if (alpha.useGlobalTween)
                {
                    alpha.tween.Copy(globalTween);
                }

                if (rectTransform)
                {
                    if (_canvasGroup)
                        LeanTween.alpha(_canvasGroup, reverse ? alpha.initialState : alpha.finalState
                            , alpha.tween.randomDuration ? Random.Range(alpha.tween.minRandDuration, alpha.tween.maxRandDuration) : alpha.tween.duration)
                            .setDelay(alpha.tween.randomDelay ? Random.Range(alpha.tween.minRandDelay, alpha.tween.maxRandDelay) : alpha.tween.delay)
                            .setEase(alpha.tween.type)
                            .setOnComplete(() => onCompleteAlpha());
                    else
                        LeanTween.color(rectTransform, reverse ? _imageColorInitial : _imageColorFinal
                            , alpha.tween.randomDuration ? Random.Range(alpha.tween.minRandDuration, alpha.tween.maxRandDuration) : alpha.tween.duration)
                            .setDelay(alpha.tween.randomDelay ? Random.Range(alpha.tween.minRandDelay, alpha.tween.maxRandDelay) : alpha.tween.delay)
                            .setEase(alpha.tween.type)
                            .setOnComplete(() => onCompleteAlpha());
                }
                else
                {
                    LeanTween.alpha(objectToAffect, reverse ? alpha.initialState : alpha.finalState
                        , alpha.tween.randomDuration ? Random.Range(alpha.tween.minRandDuration, alpha.tween.maxRandDuration) : alpha.tween.duration)
                        .setDelay(alpha.tween.randomDelay ? Random.Range(alpha.tween.minRandDelay, alpha.tween.maxRandDelay) : alpha.tween.delay)
                        .setEase(alpha.tween.type)
                        .setOnComplete(() => onCompleteAlpha());
                }
            }
        }
        #endregion

        #region Size
        if (!ignoreSize && rectTransform)
        {
            tweensExecuting++;

            if (!size.ignoreInitialState)
            {
                rectTransform.sizeDelta = reverse ? size.finalState : size.initialState;
            }

            if (size.useGlobalTween)
            {
                size.tween.Copy(globalTween);
            }

            StartCoroutine(TweenSize(reverse ? size.initialState : size.finalState));
        }
        #endregion
    }
    private IEnumerator TweenSize(Vector3 finalState)
    {
        var tempGo = new GameObject("[Temp]" + objectToAffect.name);

        tempGo.transform.SetParent(objectToAffect.transform);
        tempGo.transform.localScale = rectTransform.sizeDelta;

        yield return new WaitForSeconds(size.tween.randomDelay ? Random.Range(size.tween.minRandDelay, size.tween.maxRandDelay) : size.tween.delay);

        LeanTween.scale(tempGo, finalState, size.tween.duration).setEase(size.tween.type);

        var t = 0F;

        while (t < size.tween.duration)
        {
            t += Time.deltaTime;

            rectTransform.sizeDelta = tempGo.transform.localScale;

            yield return null;
        }

        rectTransform.sizeDelta = tempGo.transform.localScale;

        Destroy(tempGo);

        oncompleteSize();
    }
    private IEnumerator PlayAudio()
    {
        yield return new WaitForSeconds(audioDelay);

        audioSource.PlayOneShot(audioSource.clip);
    }

    public void CompleteMove() { CompleteOne(); }
    public void CompleteRotate() { CompleteOne(); }
    public void CompleteScale() { CompleteOne(); }
    public void CompleteAlpha() { CompleteOne(); }
    public void CompleteSize() { CompleteOne(); }
    public void CompleteAll()
    {
        if (deactivateAfter)
        {
            objectToAffect.SetActive(false);
        }
        else
        {
            if (rectTransform != null)
            {
                var scrolls = rectTransform.GetComponentsInChildren<ScrollRect>(true);

                for (int i = 0; i < scrolls.Length; i++)
                {
                    scrolls[i].enabled = true;
                }
            }
        }

        onCompleteAll();
    }
    public void Finish() { }
    private void CompleteOne()
    {
        tweensExecuting--;

        if (tweensExecuting < 1)
            CompleteAll();
    }
}

[System.Serializable]
public class TransitTo
{
    public Vector3 initialState;
    public Vector3 finalState;
    public TweenParameters tween;

    public bool ignoreInitialState, localSpace, useGlobalTween;
}
[System.Serializable]
public class TransitAlpha
{
    public TransitAlpha()
    {
        tween = new TweenParameters();
    }

    public float initialState;
    public float finalState;
    public TweenParameters tween;

    public bool ignoreInitialState, useGlobalTween;
}
[System.Serializable]
public class TweenParameters
{
    public LeanTweenType type = LeanTweenType.linear;
    public float delay, minRandDelay, maxRandDelay=1, duration = 1, minRandDuration, maxRandDuration=1;
    public bool randomDelay, randomDuration;

    public void Copy(TweenParameters from)
    {
        this.type = from.type;
        this.duration = from.duration;
        this.delay = from.delay;
        this.randomDelay = from.randomDelay;
        this.minRandDelay = from.minRandDelay;
        this.maxRandDelay = from.maxRandDelay;
        this.randomDuration = from.randomDuration;
        this.minRandDuration = from.minRandDuration;
        this.maxRandDuration = from.maxRandDuration;
    }
}
