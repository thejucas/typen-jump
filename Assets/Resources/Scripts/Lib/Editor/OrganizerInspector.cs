﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Organizer))]
public class OrganizerInspector : Editor
{
    private Organizer _target;

    private void OnEnable()
    {
        _target = (Organizer)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        GUILayout.Space(5);

        if (GUILayout.Button("Organize"))
        {
            _target.Organize();
        }
    }
}
