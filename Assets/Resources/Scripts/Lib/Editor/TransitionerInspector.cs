﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(Transitioner))]
public class TransitionerInspector : Editor
{
    private Transitioner _target;

    private bool _typeDefined;

    private void OnEnable()
    {
        _target = (Transitioner)target;
    }

    public override void OnInspectorGUI()
    {
        if (!_typeDefined && _target.objectToAffect)
        {
            _typeDefined = true;

            _target.rectTransform = _target.objectToAffect.GetComponent<RectTransform>();
        }

        //EditorGUILayout.IntField("Tweens Executing", _target.tweensExecuting);

        GUILayout.Space(10);

        _target.objectToAffect = (GameObject) EditorGUILayout.ObjectField("Object to Affect", _target.objectToAffect, typeof(GameObject), true);

        GUILayout.Space(10);
        GUILayout.BeginVertical("box");
        GUI.enabled = Application.isPlaying && _target.tweensExecuting < 1;
        GUILayout.BeginHorizontal("box");
        {
            if (GUILayout.Button("Play"))
            {
                _target.Play();
            }

            if (GUILayout.Button("Open"))
            {
                _target.Open();
            }

            if (GUILayout.Button("Close"))
            {
                _target.Close();
            }

            if (GUILayout.Button("Close No Deactivate"))
            {
                _target.StartTransition(true);
            }
        }
        GUILayout.EndHorizontal();
        GUI.enabled = true;

        if (!Application.isPlaying)
            EditorGUILayout.HelpBox("You have to be on 'Play Mode' in order to test the transitions", MessageType.Info);

        GUILayout.EndVertical();

        GUILayout.Space(10);

        _target.playOnAwake = EditorGUILayout.Toggle("Play On Awake", _target.playOnAwake);

        GUILayout.Space(5);

        _target.deactivateAfter = EditorGUILayout.Toggle("Deactivate After", _target.deactivateAfter);

        GUILayout.Space(5);

        _target.isOpen = EditorGUILayout.Toggle("Is Open", _target.isOpen);

        GUILayout.Space(5);

        _target.wrapMode = (Transitioner.WrapMode) EditorGUILayout.EnumPopup("Wrap Mode", _target.wrapMode);

        GUILayout.Space(5);

        _target.foldAudio = EditorGUILayout.Foldout(_target.foldAudio, "Audio");

        if (_target.foldAudio)
        {
            GUILayout.BeginVertical("box");
            {
                _target.audioSource = (AudioSource)EditorGUILayout.ObjectField("Audio Source", _target.audioSource, typeof(AudioSource), true);

                _target.audioDelay = EditorGUILayout.Slider("Audio Delay", _target.audioDelay, 0, 3);
            }
            GUILayout.EndVertical();
        }

        GUILayout.Space(5);

        _target.foldGlobalTween = EditorGUILayout.Foldout(_target.foldGlobalTween, "Global Tween");

        if (_target.foldGlobalTween)
            DrawTween(_target.globalTween);

        GUILayout.Space(5);

        DrawEditor("Position", _target.position, ref _target.foldPosition, ref _target.ignorePosition);
        DrawEditor("Rotation", _target.rotation, ref _target.foldRotation, ref _target.ignoreRotation);
        DrawEditor("Scale", _target.scale, ref _target.foldScale, ref _target.ignoreScale);

        if (_target.rectTransform)
            DrawEditor("Size", _target.size, ref _target.foldSize, ref _target.ignoreSize);

        _target.scale.localSpace = true;

        #region Alpha
        _target.foldAlpha = EditorGUILayout.Foldout(_target.foldAlpha, "Alpha");

        if (_target.foldAlpha)
        {
            EditorGUILayout.BeginVertical("box");
            _target.ignoreAlpha = EditorGUILayout.Toggle("Ignore Alpha", _target.ignoreAlpha);

            if (!_target.ignoreAlpha)
            {
                if (_target.alpha == null)
                    _target.alpha = new TransitAlpha();

                _target.alpha.ignoreInitialState = EditorGUILayout.Toggle("Ignore Initial State", _target.alpha.ignoreInitialState);

                if (!_target.alpha.ignoreInitialState)
                    _target.alpha.initialState = EditorGUILayout.Slider("Initial State", _target.alpha.initialState, 0, 1);

                _target.alpha.finalState = EditorGUILayout.Slider("Final State", _target.alpha.finalState, 0, 1);

                _target.alpha.useGlobalTween = EditorGUILayout.Toggle("Use Global Tween", _target.alpha.useGlobalTween);

                if (_target.alpha.useGlobalTween)
                {
                    GUI.enabled = false;

                    _target.alpha.tween.Copy(_target.globalTween);
                }

                DrawTween(_target.alpha.tween);

                GUI.enabled = true;
            }
            EditorGUILayout.EndVertical();
        }

        #endregion
    }

    private void DrawEditor(string name, TransitTo transitTo, ref bool fold, ref bool ignore)
    {
        fold = EditorGUILayout.Foldout(fold, name);

        if (fold)
        {
            EditorGUILayout.BeginVertical("box");
            ignore = EditorGUILayout.Toggle("Ignore " + name, ignore);

            if (!ignore)
            {
                DrawTransitToEditor(transitTo);
            }
            EditorGUILayout.EndVertical();
        }
    }
    private void DrawTween(TweenParameters tween)
    {
        GUILayout.BeginVertical("box");

        tween.type = (LeanTweenType)EditorGUILayout.EnumPopup("Tween Type", tween.type);

        GUILayout.Space(5);

        tween.randomDelay = EditorGUILayout.Toggle("Use Random Delay", tween.randomDelay);

        if (tween.randomDelay)
        {
            tween.minRandDelay = EditorGUILayout.Slider("Min Rand. Delay", tween.minRandDelay, 0, tween.maxRandDelay);
            tween.maxRandDelay = EditorGUILayout.Slider("Max Rand. Delay", tween.maxRandDelay, tween.minRandDelay, 3);
        }
        else
        {
            tween.delay = EditorGUILayout.Slider("Tween Delay", tween.delay, 0, 3);
        }

        GUILayout.Space(5);

        tween.randomDuration = EditorGUILayout.Toggle("Use Random Duration", tween.randomDuration);

        if (tween.randomDuration)
        {
            tween.minRandDuration = EditorGUILayout.Slider("Min Rand. Duration", tween.minRandDuration, 0, tween.maxRandDuration);
            tween.maxRandDuration = EditorGUILayout.Slider("Max Rand. Duration", tween.maxRandDuration, tween.minRandDuration, 5);
        }
        else
        {
            tween.duration = EditorGUILayout.Slider("Tween Duration", tween.duration, 0, 5);
        }

        GUILayout.EndVertical();
    }
    private void DrawTransitToEditor(TransitTo transitTo)
    {
        transitTo.ignoreInitialState = EditorGUILayout.Toggle("Ignore Initial State", transitTo.ignoreInitialState);

        transitTo.localSpace = EditorGUILayout.Toggle("Use Local Space", transitTo.localSpace);

        if (!transitTo.ignoreInitialState)
            transitTo.initialState = EditorGUILayout.Vector3Field("Initial State", transitTo.initialState);

        transitTo.finalState = EditorGUILayout.Vector3Field("Final State", transitTo.finalState);

        transitTo.useGlobalTween = EditorGUILayout.Toggle("Use Global Tween", transitTo.useGlobalTween);

        if (transitTo.useGlobalTween)
        {
            GUI.enabled = false;

            transitTo.tween.Copy(_target.globalTween);
        }

        DrawTween(transitTo.tween);

        GUI.enabled = true;
    }
}
