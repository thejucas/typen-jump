﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Organizer : MonoBehaviour
{
    public Vector3 size, space;
    public bool freezeX, freezeY, freezeZ;
    public Transform[] objects;

    public void Organize()
    {
        var unitySpace = size + space;
        var groupSize = unitySpace * (objects.Length - 1);
        var startPosition = -groupSize / 2;
 
        for (int i = 0; i < objects.Length; i++)
        {
            var np = startPosition + unitySpace * i;
            var po = objects[i].localPosition;

            if (freezeX)
                np.x = po.x;
            if (freezeY)
                np.y = po.y;
            if (freezeZ)
                np.z = po.z;

            objects[i].localPosition = np;
        }
    }
}
