﻿using UnityEngine;
using System.Collections;
public class TimeController : MonoBehaviour
{
    public static float timeScale { get { return _timeScale; } }
    private static float _timeScale = 1;

    public static float deltaTime { get { return Time.deltaTime * timeScale; } }
    public static float fixedDeltaTime { get { return Time.fixedDeltaTime * timeScale; } }
    public static bool GamePaused { get { return timeScale < 1; } }
    public static bool UIBlocked;

    public GameObject goMenuPause;

    public delegate void TimeScaleChanged();
    public TimeScaleChanged OnTimeScaleChanged;
    

    public void Empty() { }

    public void Init()
    {
        OnTimeScaleChanged = Empty;
        UIBlocked = false;
    }
    public void SetTimeScale(float scale)
    {
        _timeScale = scale;

        OnTimeScaleChanged();
    }

    public void BlockUI(bool active)
    {
        SetTimeScale(active ? 0 : 1);
        UIBlocked = active;
    }

    public void SetPause(bool pause, bool raiseInterface = true)
    {
        SetTimeScale(pause ? 0 : 1);

        if (goMenuPause)
            goMenuPause.SetActive(pause ? raiseInterface : false);
    }
    public static IEnumerator WaitForSeconds(float timer)
    {
        while (timer > 0)
        {
            timer -= TimeController.deltaTime;
            yield return null;
        }
    }
}
