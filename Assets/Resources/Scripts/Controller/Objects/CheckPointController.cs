﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointController : LocalMonoBehaviour
{
    public bool isActived;
    public SpriteRenderer spriteRenderer;
    public Sprite onSprite;

    private AudioSource audioSource;

    private void Start()
    {
        this.audioSource = this.gameObject.GetComponent<AudioSource>();
        if (isActived)
        {
            ChangeStatus();
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && !isActived)
        {
            isActived = true;
            this.audioSource.Play();
            ChangeStatus();
        }
    }

    private void ChangeStatus()
    {
        if (GameManager)
        {
            GameManager.actualCheckPoint = this.transform;
        }
        
        spriteRenderer.sprite = onSprite;
    }
}
