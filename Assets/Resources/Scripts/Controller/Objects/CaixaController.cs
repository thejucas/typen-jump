﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaixaController : MonoBehaviour
{
    [Header("References")]
    public SpriteRenderer spRenderer;
    public Collider mainCollider;
    public List<Sprite> sprites;
    public GameObject explosion;

    void Start ()
    {
        var index = UnityEngine.Random.Range(0, sprites.Count);
        spRenderer.sprite = sprites[index];
	}

    public void Explode()
    {
        spRenderer.enabled = false;
        if (!mainCollider)
        {
            mainCollider = this.gameObject.GetComponent<Collider>();
        }
        mainCollider.enabled = false;
        Instantiate(explosion, this.transform.position, this.transform.rotation);
    }

    public void Destroied()
    {
        Explode();
    }
}
