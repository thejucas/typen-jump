﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionController : LocalMonoBehaviour
{
    public AudioSource audioSource;
    public List<AudioClip> audios;

    private void Awake()
    {
        var audio = audios[UnityEngine.Random.Range(0, audios.Count)];
        audioSource.clip = audio;
        audioSource.Play();

        StartCoroutine(DestroySelf(audio.length));
    }

    private IEnumerator DestroySelf(float length)
    {
        yield return new WaitForSeconds(length);
        Destroy(this.gameObject);
    }
}
