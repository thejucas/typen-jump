﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EspinhoController : LocalMonoBehaviour
{
    [Header("Config")]
    public float timeHided;
    public float timeShowed;
    public float volume;

    [Header("References")]
    public Animator animator;
    
    public bool isHide;

    private AudioSource audioSource;

    private void Start()
    {
        audioSource = this.gameObject.GetComponent<AudioSource>();
        audioSource.volume = volume;
    }

    public void Hidded()
    {
        this.isHide = true;
        StartCoroutine(ShowEspinhos());
    }


    public void Showed()
    {
        audioSource.Play();
        StartCoroutine(HideEspinhos());
    }

    private IEnumerator ShowEspinhos()
    {
        yield return new WaitForSeconds(timeHided);

        
        this.isHide = false;
        this.animator.SetTrigger("Show");        
    }

    private IEnumerator HideEspinhos()
    {
        yield return new WaitForSeconds(timeShowed);

        this.animator.SetTrigger("Hide");
    }
}
