﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrilController : LocalMonoBehaviour
{
    [Header("References")]
    public SpriteRenderer spRenderer;
    public Collider mainCollider;
    public GameObject explosion;

    [Header("Caixas")]
    public List<GameObject> caixas;
    

    public void Explode()
    {
        spRenderer.enabled = false;
        mainCollider.enabled = false;
        Instantiate(explosion, this.transform.position, this.transform.rotation);

        if (caixas.Count > 0)
        {
            foreach (var item in caixas)
            {
                item.GetComponent<CaixaController>().Destroied();
            }
        }
    }
}
