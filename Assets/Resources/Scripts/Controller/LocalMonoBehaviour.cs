﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ES_Lib;

public class LocalMonoBehaviour : MonoBehaviour 
{
    public static GameController GameManager
    {
        get
        {
            if (gameManager == null)
            {
                gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
            }
            return gameManager;
        }
        set { gameManager = value; }
    }
    private static GameController gameManager;

    public static PlayerController PlayerManager
    {
        get
        {
            return playerManager;
        }
        set { playerManager = value; }
    }
    private static PlayerController playerManager;
    

    public TimeController TimeManager
    {
        get
        {
            if (timeManager == null)
            {
                timeManager = GameManager.GetComponentInChildren<TimeController>();

                if (timeManager == null)
                {
                    var go = new GameObject("TimeController");
                    go.transform.SetParent(GameManager.transform);

                    timeManager = go.AddComponent<TimeController>();
                }
            }
            return timeManager;
        }
        set { timeManager = value; }
    }
    private TimeController timeManager;

    public CanvasController CanvasManager
    {
        get
        {
            if (canvasManager == null)
            {
                canvasManager = FindObjectOfType<CanvasController>();
            }
            return canvasManager;
        }
        set { canvasManager = value; }
    }
    private CanvasController canvasManager;
}
