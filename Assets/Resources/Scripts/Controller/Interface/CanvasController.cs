﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CanvasController : LocalMonoBehaviour
{
    [Header("Interface")]
    public CanvasGroup menuCanvas;
    public CanvasGroup gameCanvas;
    public CanvasGroup victoryCanvas;
    public CanvasGroup creditsCanvas;
    public List<TextMesh> textMesh;
    
    [Header("Config")]
    public float timeToTween;

    [Header("Victory")]
    public Text commandsCount;
    public Text timePlayedCount;

    void Start ()
    {
        CanvasManager = this;
        
        this.menuCanvas.alpha = GameManager.showMenu ? 1 : 0;
        this.gameCanvas.alpha = GameManager.showMenu ? 0 : 1;
        this.victoryCanvas.alpha = 0;
        this.creditsCanvas.alpha = 0;

        foreach (var item in textMesh)
        {
            LeanTween.alpha(item.gameObject, GameManager.showMenu ? 0 : 1, 0);
        }
    }

    

    public void LoadMenu()
    {
        ChangeScene(menu: true);
    }

    public void LoadCredits()
    {
        ChangeScene(credits: true);
    }



    public void LoadGame()
    {
        ChangeScene(game: true);
        GameManager.canMove = true;
        GameManager.StartPlayTime = Time.time;

        foreach (var item in textMesh)
        {
            LeanTween.alpha(item.gameObject, 1, 1);
        }
    }

    public void LoadVictory()
    {
        GameManager.canMove = false;

        ActiveScene(victory: true);
        
        var time = TimeSpan.FromSeconds(Time.time - GameManager.StartPlayTime);
        
        if (time.Hours > 0)
        {
            GameManager.TimePlayed = string.Format("{0}:{1}:{2}", time.Hours.ToString().Length > 1 ? time.Hours.ToString() : "0" + time.Hours.ToString(), time.Minutes.ToString().Length > 1 ? time.Minutes.ToString() : "0" + time.Minutes.ToString(), time.Seconds.ToString().Length > 1 ? time.Seconds.ToString() : "0" + time.Seconds.ToString());
        }
        else
        {
            GameManager.TimePlayed = string.Format("{0}:{1}", time.Minutes.ToString().Length > 1 ? time.Minutes.ToString() : "0" + time.Minutes.ToString(), time.Seconds.ToString().Length > 1 ? time.Seconds.ToString() : "0" + time.Seconds.ToString());
        }

        commandsCount.text = " " + GameManager.CommandCount.ToString();
        timePlayedCount.text = " " + GameManager.TimePlayed;

        this.GetComponent<AudioSource>().Play();
    }

    public void ExitGame()
    {
        if (Application.platform != RuntimePlatform.WebGLPlayer)
        {
            Application.Quit();
        }
    }

    public void RestartGame()
    {
        StartCoroutine(LoadScene());
    }

    IEnumerator LoadScene()
    {
        yield return new WaitForSeconds(1);

        AsyncOperation async = SceneManager.LoadSceneAsync(0);

        while (!async.isDone)
        {
            yield return null;
        }
    }

    #region Private Methods

    private void ChangeScene(bool menu = false, bool game = false, bool victory = false, bool credits = false)
    {
        var showedCanvas = GetOpenCanvas();
        var nextCanvas = GetNextCanvas(menu, game, credits);

        LeanTween.alpha(showedCanvas, 0, timeToTween).setOnComplete(() => 
        {
            creditsCanvas.interactable = credits;
            LeanTween.alpha(nextCanvas, 1, timeToTween);
        });
    }

    private void ActiveScene(bool menu = false, bool game = false, bool victory = false, bool credits = false)
    {
        var showedCanvas = GetOpenCanvas();
        var nextCanvas = GetNextCanvas(menu, game, credits);

        LeanTween.alpha(showedCanvas, 0, timeToTween);
        LeanTween.alpha(nextCanvas, 1, timeToTween);
    }

    private CanvasGroup GetNextCanvas(bool menu, bool game, bool credits)
    {
        CanvasGroup canvas;

        if (menu)
        {
            canvas = this.menuCanvas;
        }
        else if (game)
        {
            canvas = this.gameCanvas;
        }
        else if(credits)
        {
            canvas = this.creditsCanvas;
        }
        else
        {
            canvas = this.victoryCanvas;
        }

        return canvas;
    }

    private CanvasGroup GetOpenCanvas()
    {
        CanvasGroup canvas;

        if (this.menuCanvas.alpha == 1)
        {
            canvas = this.menuCanvas;
        }
        else if (this.gameCanvas.alpha == 1)
        {
            canvas = this.gameCanvas;
        }
        else
        {
            canvas = this.creditsCanvas;
        }

        return canvas;
    }

    #endregion
}
