﻿using System.Collections;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using UnityEngine;

public class CreditsController : LocalMonoBehaviour
{
    public void OpenUrl(string url)
    {
        OpenPage(url);
    }

    private void OpenPage(string url)
    {
#if !UNITY_WEBGL
        Application.OpenURL(url);
#else
        OpenLinkJSPlugin(url);
#endif
    }

    public void OpenLinkJSPlugin(string url)
    {
#if !UNITY_EDITOR
		openWindow(url);
#endif
    }

    [DllImport("__Internal")]
    private static extern void openWindow(string url);
}