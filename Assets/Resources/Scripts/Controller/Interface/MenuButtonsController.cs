﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuButtonsController : MonoBehaviour
{
    public GameObject webGLBtns;
    public GameObject othersBtns;

    void Awake ()
    {
        webGLBtns.SetActive(Application.platform == RuntimePlatform.WebGLPlayer);
        othersBtns.SetActive(Application.platform != RuntimePlatform.WebGLPlayer);
	}
}
