﻿using Com.LuisPedroFonseca.ProCamera2D;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class CommandController : LocalMonoBehaviour
{
    [Header("Inputs")]
    public List<CommandInput> inputs;

    [Header("References")]
    public Text commandText;
    public CanvasGroup canvasGroup;
    public bool shakeCamera;
    public bool countCommands;

    [Header("Font Size Adjust")]
    public Text underline;
    public Text space;

    private AudioSource source;
    private int fontSize { get { return (int)Math.Ceiling((Screen.height / 10f) * 0.6f); } }
    private void Awake()
    {
        this.commandText.fontSize = (Screen.height / 10);
        this.underline.fontSize = (Screen.height / 10);
        
        //Force all word in low case
        inputs.ForEach((CommandInput input) => { input.Words.ForEach((string word) => { word = word.ToLower(); });  });

        this.source = this.gameObject.GetComponent<AudioSource>();
    }

    private void OnRectTransformDimensionsChange()
    {
        this.commandText.fontSize = fontSize;
        this.commandText.resizeTextMinSize = fontSize;
        this.commandText.resizeTextMaxSize = fontSize;
        this.underline.fontSize = fontSize;
        this.space.fontSize = fontSize;
    }

    void Update()
    {
        if (canvasGroup.alpha == 0)
            return;

        //Escrever
        for (int i = 97; i < 123; i++)
        {
            if (Input.GetKeyDown((KeyCode)i))
            {
                if (commandText.text.Length >= GameManager.limitCommandText)
                {
                    RunAudio(GameManager.wrong, 1);
                }
                else
                {
                    commandText.text += (KeyCode)i;
                    RunAudio(GameManager.typing, 1);
                }
            }
        }

        //Apagar
        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            var command = commandText.text;
            if (!string.IsNullOrEmpty(command))
            {
                commandText.text = command.Remove(command.Length - 1);
            }
        }

        //Espaço
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (commandText.text.Length >= GameManager.limitCommandText)
            {
                RunAudio(GameManager.wrong, 1);
            }
            else
            {
                commandText.text += " ";
                RunAudio(GameManager.typing, 1);
            }
        }

        //Submitar
        if (Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Return))
        {
            var command = commandText.text;

            CommandSubmited(command);

            if (VerifyCommand(command))
            {
                RunAudio(GameManager.correct, 0.5f);

                if (countCommands)
                    GameManager.CommandCount++;
            }
            else
            {
                RunAudio(GameManager.wrong, 1);

                if (shakeCamera)
                    ProCamera2DShake.Instance.Shake();
            }

            ClearInput();
        }
    }

    public virtual void CommandSubmited(string command)
    {

    }

    public bool VerifyCommand(string command)
    {
        foreach (var input in inputs)
        {
            if (input.Words.Contains(command.ToLower()))
            {
                input.Method.Invoke();

                return true;
            }
        }

        return false;
    }

    #region Private Methods

    public void ClearInput()
    {
        commandText.text = "";
    }
    
    private void RunAudio(AudioClip audio, float volume)
    {
        this.source.clip = audio;
        this.source.volume = volume;
        this.source.Play();
    }

    #endregion
}

[Serializable]
public class CommandInput
{
    public string name;
    [Header("Palavras aceitas pelo input")]
    public List<string> Words;
    [Header("Método a ser chamado quando Input realizado")]
    public UnityEvent Method;
}