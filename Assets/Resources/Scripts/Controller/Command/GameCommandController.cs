﻿using Com.LuisPedroFonseca.ProCamera2D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GameCommandController : CommandController
{
    [Header("Input Field")]
    public GameObject helpPanel;
    
    public override void CommandSubmited(string command)
    {
        base.CommandSubmited(command);

        if (this.helpPanel.activeSelf && command != "help")
        {
            this.helpPanel.SetActive(false);
        }
    }

    public void Help()
    {
        this.helpPanel.SetActive(!this.helpPanel.activeSelf);
    }

    public void CloseHelp()
    {
        this.helpPanel.SetActive(false);
    }
}
