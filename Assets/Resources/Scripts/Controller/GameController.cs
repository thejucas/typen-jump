﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections.Generic;

public class GameController : LocalMonoBehaviour
{
    [Header("Configs")]
    public bool showMenu;
    public int limitCommandText;

    [Header("Command Audios")]
    public AudioClip wrong;
    public AudioClip typing;
    public AudioClip correct;
    

    internal bool canMove;

    internal int CommandCount;
    internal float StartPlayTime;
    internal string TimePlayed;
    internal Transform actualCheckPoint;
    

    public static string DataPath { get { return Application.dataPath + "/Data/"; } }
    
    private void Awake()
    {
        this.tag = "GameController";
        this.name = "Game Controller";
        
        canMove = !showMenu;

        GameManager = this;

        if (TimeManager)
        {
            TimeManager.Init();
            TimeManager.SetPause(false);
        }
    }
    
    #region Shared Methods

    private void LoadingScene(string name)
    {
        StartCoroutine(LoadNewScene(name));
    }

    #endregion

    #region Private Methods

    IEnumerator LoadNewScene(string name)
    {
        yield return new WaitForSeconds(5);

        AsyncOperation async = SceneManager.LoadSceneAsync(name);

        while (!async.isDone)
        {
            yield return null;
        }
    }

    #endregion
}