﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotController : LocalMonoBehaviour
{
    [Header("Configs")]
    public float speed;
    public LayerMask layer;

    [Header("Audios")]
    public AudioSource audioSource;
    public List<AudioClip> audios;

    private bool isLeft;
    private bool isDestroied;

    void Update ()
    {
        if (!isDestroied)
        {
            this.transform.Translate(Vector3.right * Time.timeScale * speed);
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        if (((1 << other.gameObject.layer) & layer) != 0 && !isDestroied)
        {
            if (other.gameObject.tag == "Barril")
            {
                other.gameObject.GetComponent<BarrilController>().Explode();
                StartCoroutine(WaitToDestroy(0));
            }
            else
            {
                isDestroied = true;
                GetComponent<SpriteRenderer>().enabled = false;

                var hit = audios[UnityEngine.Random.Range(0, audios.Count)];
                audioSource.clip = hit;
                audioSource.Play();

                StartCoroutine(WaitToDestroy(hit.length));
            }
        }
    }

    private IEnumerator WaitToDestroy(float length)
    {
        yield return new WaitForSeconds(length);

        Destroy(this.gameObject);
    }


    internal void StartShot(bool isLeft)
    {
        this.isLeft = isLeft;
        if (isLeft)
        {
            this.transform.Rotate(0, 0, 180);
        }
    }
}
