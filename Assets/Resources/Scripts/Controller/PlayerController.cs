using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerController : LocalMonoBehaviour
{
    [Header("References")]
    public Transform body;
    public Transform gun;
    public Transform shotPostion;
    public LayerMask staticLayer;
    public GameObject shot;
    public Animator playerAnimator;
    public Animator gunAnimator;

    [Header("Audios")]
    public AudioSource audioSource;
    public AudiosPlayer audios;
    
    [Header("Configs")]
    public float gravity = 20;
    public float runSpeed = 12;
    public float acceleration = 30;

    [Header("Jump")]
    public int jumpSpeed;
    public int jumpAcceleration;
    public float jumpHeight = 12;


    private Animator animator;
    private Vector3 amountToMove;
    private CharacterController _characterController;

    private float currentSpeed;
    private bool isMoving;
    private bool isLeft;
    private bool isGround;
    private bool isJump;
    private float moveLeftCounter;
    private float moveRightCounter;

    private readonly List<string> objectsStop = new List<string>()
    {
        "Ground",
        "CaixaMadeira",
        "CaixaMetal",
        "Barril"
    };

    void Awake()
    {
        PlayerManager = this;
        _characterController = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();

        isMoving = false;
        isLeft = false;
    }

    void Update()
    {
        if (GameManager.canMove)
        {
            if (Application.platform == RuntimePlatform.WindowsEditor)
            {
                if ((Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)))
                {
                    Jump();
                }

                if (Input.GetAxis("Horizontal") > 0)
                {
                    WalkRight();
                }

                if (Input.GetAxis("Horizontal") < 0)
                {
                    WalkLeft();
                }

                if (Input.GetKeyDown(KeyCode.Space))
                {
                    Fire();
                }

                if (Input.GetKeyDown(KeyCode.DownArrow))
                {
                    Stop();
                }
            }

            if (isMoving)
            {
                var targetSpeed = (isLeft ? -1 : 1) * runSpeed;
                currentSpeed = IncrementTowards(currentSpeed, targetSpeed, acceleration);
            }
            else
            {
                if (isJump && !isGround)
                {
                    var targetSpeed = (isLeft ? -1 : 1) * jumpSpeed;
                    currentSpeed = IncrementTowards(currentSpeed, targetSpeed, jumpAcceleration);
                }
                else
                {    
                    currentSpeed = 0;
                }
            }

            #region Jump

            RaycastHit hitDown;
            Ray rayDown = new Ray(transform.localPosition, -transform.up);

            Debug.DrawLine(rayDown.origin, rayDown.origin + (Vector3.down * 0.35f), Color.red);

            if (Physics.Raycast(rayDown, out hitDown, 0.35f, staticLayer))
            {
                isGround = true;
                isJump = false;
                playerAnimator.SetBool("IsGround", true);
            }
            else
            {
                isGround = false;
                isJump = true;
                playerAnimator.SetBool("IsGround", false);
            }

            if (!isGround)
            {
                amountToMove.y -= gravity * Time.deltaTime;
            }

            #endregion
            
            #region Hit Right

            var rayPosition = new Vector3(transform.localPosition.x, transform.localPosition.y - 0.02f, transform.localPosition.z);

            RaycastHit hitRight;
            Ray rayRight = new Ray(rayPosition, transform.right);

            Debug.DrawLine(rayRight.origin, rayRight.origin + (Vector3.right * 0.35f), Color.blue);

            if (Physics.Raycast(rayRight, out hitRight, 0.35f, staticLayer))
            {
                if (objectsStop.Contains(hitRight.collider.gameObject.tag) && isMoving && !isLeft && isGround)
                {
                    if (isMoving && moveRightCounter <= 0)
                    {
                        currentSpeed = 0;
                        Stop();
                        moveRightCounter = 3f;
                    }
                    else
                    {
                        moveRightCounter -= Time.timeScale;
                    }
                }
            }

            #endregion

            #region Hit Left

            RaycastHit hitLeft;
            Ray rayLeft = new Ray(rayPosition, -transform.right);
            Debug.DrawLine(rayLeft.origin, rayLeft.origin + (Vector3.left * 0.35f), Color.green);

            if (Physics.Raycast(rayLeft, out hitLeft, 0.35f, staticLayer))
            {
                if (objectsStop.Contains(hitLeft.collider.gameObject.tag) && isMoving && isLeft && isGround)
                {
                    if (isMoving && moveLeftCounter <= 0)
                    {
                        currentSpeed = 0;
                        Stop();
                        moveLeftCounter = 3f;
                    }
                    else
                    {
                        moveLeftCounter -= Time.timeScale;
                    }
                }
            }

            // Reset z
            if (transform.position.z != 0)
            {
                amountToMove.z = -transform.position.z;
            }

            #endregion

            // Set amount to move
            amountToMove.x = currentSpeed;

            if (amountToMove.x != 0)
            {
                body.localScale = new Vector2(Mathf.Sign(amountToMove.x) * Mathf.Abs(body.localScale.x), body.localScale.y);
                gun.localScale = new Vector2(Mathf.Sign(amountToMove.x) * Mathf.Abs(body.localScale.x), body.localScale.y);
            }

            _characterController.Move(amountToMove * Time.deltaTime);
        }
    }
    
    #region Commands Methods

    public void Jump()
    {
        if (!isJump)
        {
            RunAudio(audios.Pulo);
            this.isJump = true;
            amountToMove.y = jumpHeight;
        }
    }

    public void WalkLeft()
    {
        this.isLeft = true;
        this.isMoving = true;
        gunAnimator.SetBool("IsWalk", true);
        playerAnimator.SetBool("IsWalk", true);
    }

    public void WalkRight()
    {
        this.isLeft = false;
        this.isMoving = true;
        gunAnimator.SetBool("IsWalk", true);
        playerAnimator.SetBool("IsWalk", true);
    }

    public void Stop()
    {
        this.isMoving = false;
        this.isJump = false;
        gunAnimator.SetBool("IsWalk", false);
        playerAnimator.SetBool("IsWalk", false);
    }

    public void Fire()
    {
        RunAudio(audios.Tiro);
        InstantiateShot();
        gunAnimator.SetTrigger("Fire");
    }

    public void Turn()
    {
        if (isLeft)
            TurnRight();
        else
            TurnLeft();
    }

    public void TurnRight()
    {
        this.isLeft = false;

        body.localScale = new Vector2(1 * Mathf.Abs(body.localScale.x), body.localScale.y);
        gun.localScale = new Vector2(1 * Mathf.Abs(body.localScale.x), body.localScale.y);
    }

    public void TurnLeft()
    {
        this.isLeft = true;

        body.localScale = new Vector2((-1) * Mathf.Abs(body.localScale.x), body.localScale.y);
        gun.localScale = new Vector2((-1) * Mathf.Abs(body.localScale.x), body.localScale.y);
    }

    public void JumpRight()
    {
        TurnRight();
        Jump();
    }

    public void JumpLeft()
    {
        TurnLeft();
        Jump();
    }

    #endregion

    #region Collision

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.gameObject.tag == "DeathCollider")
        {
            StartCoroutine(ResetPositionRoutine());
        }
        if (hit.gameObject.tag == "Serra")
        {
            StartCoroutine(ResetPositionRoutine());
        }
        if (hit.gameObject.tag == "Explosion")
        {
            StartCoroutine(ResetPositionRoutine());
        }
        if (hit.gameObject.tag == "Espinho")
        {
            StartCoroutine(ResetPositionRoutine());
        }
        if (hit.gameObject.tag == "Victory")
        {
            Stop();

            CanvasManager.LoadVictory();
        }
    }

    private IEnumerator ResetPositionRoutine()
    {
        //Audio
        RunAudio(audios.Morte);

        //Animacao
        playerAnimator.SetBool("IsWalk", false);
        gunAnimator.SetBool("IsWalk", false);
        
        isMoving = false;
        isJump = false;

        GameManager.canMove = false;

        yield return new WaitForEndOfFrame();

        this.transform.position = GameManager.actualCheckPoint.position;
        GameManager.canMove = true;
    }

    #endregion
    
    #region Private Methods

    // Increase n towards target by speed
    private float IncrementTowards(float n, float target, float a)
    {
        if (n == target)
        {
            return n;	
        }
        else
        {
            float dir = Mathf.Sign(target - n); 
            n += a * Time.deltaTime * dir;
            return (dir == Mathf.Sign(target - n)) ? n : target;
        }
    }

    private void InstantiateShot()
    {
        var shotGO = Instantiate(shot, shotPostion.position, transform.rotation);
        shotGO.GetComponent<ShotController>().StartShot(this.isLeft);
    }

    private void RunAudio(AudioClip audio)
    {
        this.audioSource.clip = audio;
        this.audioSource.Play();
    }

    #endregion
}

[Serializable]
public class AudiosPlayer
{
    public AudioClip Tiro;
    public AudioClip Pulo;
    public AudioClip Morte;
}