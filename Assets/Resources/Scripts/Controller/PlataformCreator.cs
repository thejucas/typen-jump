﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlataformCreator : MonoBehaviour
{
    [Header("Rerencias")]
    public Transform initialPosition;
    public LayerMask layers;

    [Header("Prefabs")]
    public List<PlataformClass> plataformBag;
    
    [Header("Distances")]
    public float horizontalDistance;
    public float verticalDistance;

    [Header("Moviment Controls")]
    public float rightTime = 5;
    public float leftTime = 5;
    public float upTime = 5;
    public float downTime = 5;

    private void Start()
    {
        if (initialPosition)
        {
            this.transform.position = initialPosition.transform.position;
        }

        rightTimeLocal = rightTime;
        leftTimeLocal = leftTime;
        upTimeLocal = upTime;
        downTimeLocal = downTime;
    }

    private float rightTimeLocal = 5;
    private float leftTimeLocal = 5;
    private float upTimeLocal = 5;
    private float downTimeLocal = 5;

    private void Update()
    {
        rightTimeLocal -= Time.timeScale;
        leftTimeLocal -= Time.timeScale;
        upTimeLocal -= Time.timeScale;
        downTimeLocal -= Time.timeScale;

        if (Input.GetKey(KeyCode.DownArrow) && downTimeLocal < 0)
        {
            downTimeLocal = downTime;
            this.transform.localPosition = new Vector3(this.transform.localPosition.x, this.transform.localPosition.y - verticalDistance, -1);
        }

        if (Input.GetKey(KeyCode.UpArrow) && upTimeLocal < 0)
        {
            upTimeLocal = upTime;
            this.transform.localPosition = new Vector3(this.transform.localPosition.x, this.transform.localPosition.y + verticalDistance, -1);
        }

        if (Input.GetKey(KeyCode.RightArrow) && rightTimeLocal < 0)
        {
            rightTimeLocal = rightTime;
            this.transform.localPosition = new Vector3(this.transform.localPosition.x + horizontalDistance, this.transform.localPosition.y, -1);
        }

        if (Input.GetKey(KeyCode.LeftArrow) && leftTimeLocal < 0)
        {
            leftTimeLocal = leftTime;
            this.transform.localPosition = new Vector3(this.transform.localPosition.x - horizontalDistance, this.transform.localPosition.y, -1);
        }

        if (Input.GetKey(KeyCode.Delete) || Input.GetKeyDown(KeyCode.Backspace))
        {
            RaycastHit hit;
            Ray ray = new Ray(transform.position, transform.forward);
            
            if (Physics.Raycast(ray, out hit, 10, layers))
            {   
                Destroy(hit.transform.gameObject);
            }
        }

        foreach (var item in plataformBag)
        {
            if (Input.GetKey(item.KeyToInstance))
            {
                StartCoroutine(PlataformConstruct(item));
            }
        }
    }
    
    private IEnumerator PlataformConstruct(PlataformClass bag)
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, transform.forward);

        if (Physics.Raycast(ray, out hit, 10, layers))
        {
            Destroy(hit.transform.gameObject);
        }

        var position = new Vector3(transform.localPosition.x, transform.localPosition.y, 0);
        var instance = Instantiate(bag.Prefab, position, transform.rotation, bag.Parent);
        instance.name = bag.NameInstance;

        yield return null;
    }
}

[Serializable]
public class PlataformClass
{
    public string name;
    public KeyCode KeyToInstance;
    public GameObject Prefab;
    public Transform Parent;
    public string NameInstance;
}